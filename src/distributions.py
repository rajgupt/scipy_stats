import numpy as np
import scipy.stats
import matplotlib.pyplot as plt

# stats distributions
from scipy import stats

allDists = ['norm', 'expon', 'binom', 'chi2', 't', 'f']

# 1. normal distribution
from scipy.stats import norm

# 2. exponential distribution
from scipy.stats import expon

# 3. binomial distribution
from scipy.stats import binom

# 4. chi-square distribution
from scipy.stats import chi2

# 5. t distribution
from scipy.stats import t

# 6. f distribution
from scipy.stats import f


##
# discussion items

# 1. Show histogram of all distributions


def plot_sample_hist(sample, title):
    '''plots the histogram for sample data'''
    plt.figure()
    plt.title(title)
    plt.hist(sample)

# mean=0, var=1, size=1000
sample = norm.rvs(size=1000)
plot_sample_hist(sample, 'normal distribution')

# lambda=1, size=1000
sample = expon.rvs(size=1000)
plot_sample_hist(sample, 'exponential distribution')

# n=10 trials for the binomial event having p=0.5 probability
sample = binom.rvs(10, 0.5, size=1000)
plot_sample_hist(sample, 'binomial distribution')

# chi-square of 10 dof
sample = chi2.rvs(10, size=1000)
plot_sample_hist(sample, 'chi-square distribution')

# t dist of 10 dof
sample = t.rvs(10, size=1000)
plot_sample_hist(sample, 't distribution')

# f dist of dof1=10 and dof2=20
sample = f.rvs(10, 20, size=1000)
plot_sample_hist(sample, 'f distribution')
